# -*- coding: utf-8 -*-

import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

import utils.averages as averages
from diagnostics.baseplot import Plotting
from diagnostics.diagnostic import Diagnostic


class DiagVspSlice(Diagnostic):
    def __init__(self, avail_vars=None, specnames=None):
        super().__init__()
        self.name = 'Velocity space slice'
        self.tabs = ['fluxtube', 'xglobal', "GENE3D"]

        self.help_txt = """ Analyze velocity space
                        \nquant : quantity to plot
                        \nspec : species (def. all)
                        \nt avg : time averaged (def. True)
                        \nSave h5 : save hdf5 file, only for time average plot (def. True)
                        \nSave pdf : save pdf file (def. False)"""

        self.avail_vars = avail_vars
        self.specnames = specnames

        self.opts = {"quant": {'tag': "Quant", 'values': None, 'files': ['vsp']},
                     "spec": {'tag': "Spec", 'values': self.list_specnames()},
                     "t_avg": {'tag': "t avg", 'values': [False, True]},
                     "save_h5": {'tag': 'Save h5', 'values': [True, False]},
                     "save_pdf": {'tag': 'Save pdf', 'values': [False, True]}}

        self.set_defaults()

        self.options_gui = Diagnostic.OptionsGUI()

    def set_options(self, run_data, species):
        """ to reduce memory, we initialize only the requested variable"""
        class VspStep:
            def __init__(self, specnames):
                for spec in specnames:
                    setattr(self, spec, [])

        self.run_data = run_data

        self.geom = run_data.geometry

        self.get_spec_from_opts()

        self.vsp_step = VspStep(self.specnames)

        self.myquant = self.opts['quant']['value']

        self.get_needed_vars(['vsp'])

        return self.needed_vars

    def execute(self, data, step, last_step):
        data = getattr(getattr(data, 'vsp'), self.myquant)(step.time, step.vsp)
        for i_spec, spec in enumerate(self.specnames):
            getattr(self.vsp_step, spec).append(data[i_spec, :, :, :])

    def plot(self, time_requested, output=None, out_folder=None):

        z_lbl = r'$z$'
        v_lbl = r'$v_{\parallel}$'
        m_lbl = r'$\mu$'

        def plot_vsp_maps(ax_zv, ax_zm, ax_vm, z, v, w, data, tm_lbl):
            plot_a_map(ax_zv, z, v, data[0, :, :], z_lbl, v_lbl, self.myquant + tm_lbl)
            plot_a_map(ax_zm, z, w, data[:, 0, :], z_lbl, m_lbl, self.myquant + tm_lbl)
            plot_a_map(ax_vm, v, w, data[:, :, 0], v_lbl, m_lbl, self.myquant + tm_lbl)

        def plot_a_map(ax, x, y, f, x_lbl, y_lbl, ttl):
            cm1 = ax.pcolormesh(x, y, np.squeeze(f))
            ax.set_rasterization_zorder(z=-10)
            ax.set_xlabel(x_lbl)
            ax.set_ylabel(y_lbl)
            ax.set_title(ttl)

        self.plotbase = Plotting()

        for spec in self.specnames:
            if self.opts['save_pdf']['value']:
                pp = PdfPages(str(out_folder) + '/vsp_slices_{}.pdf'.format(spec))
                plt.ioff()

            fig = plt.figure(figsize=(15, 10), dpi=100)
            ax_zv = fig.add_subplot(1, 3, 1)
            ax_zm = fig.add_subplot(1, 3, 2)
            ax_vm = fig.add_subplot(1, 3, 3)

            if self.opts['t_avg']['value']:

                data = averages.mytrapz(getattr(self.vsp_step, spec), time_requested)
                tm_lbl = spec +' t avg. {:.2f} - {:.2f}'.format(time_requested[0], time_requested[-1])
                plot_vsp_maps(ax_zv, ax_zm, ax_vm, self.run_data.spatialgrid.z,
                              self.run_data.vspgrid.vp, self.run_data.vspgrid.mu,
                              data, tm_lbl)

                if self.opts['save_pdf']['value']:
                    fig.savefig(pp, format='pdf', bbox_inches='tight')

                if self.opts['save_h5']['value']:
                    file = h5py.File(out_folder / 'vsp_slices_{}.h5'.format(spec), 'w')
                    file["/z"] = self.run_data.spatialgrid.z
                    file["/vp"] = self.run_data.vspgrid.vp
                    file["/mu"] = self.run_data.vspgrid.mu
                    file["/"+self.myquant] = data
                    file.close()
            else:
                ttl = self.myquant + spec
                if self.opts['save_pdf']['value']:
                    for i_t, time in enumerate(time_requested):
                        tm_lbl = spec + ', t = {:.2f}'.format(time)
                        plot_vsp_maps(ax_zv, ax_zm, ax_vm, self.run_data.spatialgrid.z,
                            self.run_data.vspgrid.vp, self.run_data.vspgrid.mu,
                            getattr(self.vsp_step, spec)[i_t], tm_lbl)
                        fig.savefig(pp, format='pdf', bbox_inches='tight')
                else:
                    data = np.array(getattr(self.vsp_step, spec))
                    fig = plt.figure(figsize=(15, 10), dpi=100)
                    ax_zv = fig.add_subplot(1, 3, 1)
                    self.plotbase.plot_contour_quant_timeseries(ax_zv, fig, self.run_data.vspgrid.vp, self.run_data.spatialgrid.z, data[:, 0, :, :],
                                    False, time_requested, v_lbl, z_lbl, ttl)
                    ax_zm = fig.add_subplot(1, 3, 2)
                    self.plotbase.plot_contour_quant_timeseries(ax_zm, fig, self.run_data.vspgrid.mu, self.run_data.spatialgrid.z, data[:, :, 0, :],
                                    False, time_requested, m_lbl, z_lbl, ttl)
                    ax_vm = fig.add_subplot(1, 3, 3)
                    self.plotbase.plot_contour_quant_timeseries(ax_vm, fig, self.run_data.vspgrid.mu, self.run_data.vspgrid.vp, data[:, :, :, 0],
                                    False, time_requested, m_lbl, v_lbl, ttl)

            if self.opts['save_pdf']['value']:
                pp.close()
            else:
                fig.show()
