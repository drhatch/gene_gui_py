#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tkinter import END

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

import utils.aux_func as aux_func
from diagnostics.baseplot import Plotting
from diagnostics.diagnostic import Diagnostic


class DiagGeometrygene3d(Diagnostic):
    def __init__(self, avail_vars=None, specnames=None):
        super().__init__()
        self.name = 'Geometry'
        self.tabs = ['GENE3D']
        self.help_txt = """Geometry coefficients in different dimensions
        \n
        \nx ind : radial position in x/a (e.g. 0.4), default middle point
        \ny ind : y positionl in terms of larmor radious, default middle point
        \nz ind : z position, default outboard midplane"""

        self.opts = {"xind": {'tag': "x ind", 'values': None},
                     "yind": {'tag': "y ind", 'values': None},
                     "zind": {'tag': "z ind", 'values': None}}
        self.avail_vars = avail_vars
        self.set_defaults()

        self.options_gui = Diagnostic.OptionsGUI()

    def set_options(self, run_data, species):

        xind = self.opts['xind']['value']
        if not xind or xind == -1:
            self.xind = int(run_data.spatialgrid.nx0/2)
        else:
            self.xind = aux_func.find_nearest(run_data.spatialgrid.x_a, float(xind))

        yind = self.opts['yind']['value']
        if not yind or yind == -1:
            self.yind = int(run_data.spatialgrid.ny0/2)
        else:
            self.yind = aux_func.find_nearest(run_data.spatialgrid.y, float(yind))

        zind = self.opts['zind']['value']
        if not zind or zind == -1:
            self.zind = int(run_data.spatialgrid.nz0/2)
        else:
            self.zind = aux_func.find_nearest(run_data.spatialgrid.z, float(zind))

        self.run_data = run_data

        self.geom = run_data.geometry

        self.get_needed_vars(['field'])

        return self.needed_vars

    def execute(self, data, step, last_step):
        """ nothing to do here """

    def plot(self, time_requested, output=None, out_folder=None):

        if output:
            output.info_txt.insert(END, "Visualizing geometric coefficients\n")

        self.plotbase = Plotting()
        self.names = ({'gxx': r"$g^{xx}$", 'gxy': r"$g^{xy}$",
                       'gxz': r"$g^{xz}$", 'gyy': r"$g^{yy}$",
                       'gyz': r"$g^{yz}$", 'gzz': r"$g^{zz}$",
                       'Bfield': "Bfield", 'dBdx': r"$dB/dx$", 'dBdy': r"$dB/dy$", 'dBdz': r"$dB/dz$",
                       'K_x': r"$K_x$", 'K_y': r"$K_y$",
                       'jacobian': r"$Jacobian$"})

        text_x = ', x=' + '{:.1f}'.format(self.run_data.spatialgrid.x_a[self.xind])
        text_y = ', y=' + '{:.1f}'.format(self.run_data.spatialgrid.y[self.yind])
        text_z = ', z=' + '{:.1f}'.format(self.run_data.spatialgrid.z[self.zind])

        x = self.run_data.spatialgrid.x_a
        y = self.run_data.spatialgrid.y
        z = self.run_data.spatialgrid.z/np.pi

        x_xz_mesh, z_xz_mesh = np.meshgrid(self.run_data.spatialgrid.x_a,
                                           self.run_data.spatialgrid.z/np.pi)
        y_yz_mesh, z_yz_mesh = np.meshgrid(self.run_data.spatialgrid.y,
                                           self.run_data.spatialgrid.z/np.pi)
        x_xy_mesh, y_xy_mesh = np.meshgrid(self.run_data.spatialgrid.x_a,
                                           self.run_data.spatialgrid.y)

        ppd = PdfPages(str(out_folder) + '/geometry.pdf')
        plt.ioff()

        for fld, lbl in self.names.items():
            figure = plt.figure(figsize=(15, 10), dpi=100)
            a1 = figure.add_subplot(2, 3, 1)
            a1.plot(z, getattr(self.geom, fld)[:, self.yind, self.xind])
            a1.set_title(lbl + text_x + text_y)
            a1.set_xlabel(r'$z/\pi$')

            a2 = figure.add_subplot(2, 3, 2)
            a2.plot(x, getattr(self.geom, fld)[self.zind, self.yind, :])
            a2.set_title(lbl + text_y + text_z)
            a2.set_xlabel(r'$x/a$')

            a3 = figure.add_subplot(2, 3, 3)
            a3.plot(y, getattr(self.geom, fld)[self.zind, :, self.xind])
            a3.set_title(lbl + text_x + text_z)
            a3.set_xlabel(r'$y/\rho_{ref}$')

            a4 = figure.add_subplot(2, 3, 4)
            a4.pcolormesh(z_xz_mesh, x_xz_mesh,
                      getattr(self.geom, fld)[:, self.yind, :])
            a4.set_title(lbl + text_y)
            a4.set_xlabel(r'$z/\pi$')
            a4.set_ylabel(r'$x/a$')

            a5 = figure.add_subplot(2, 3, 5)
            a5.pcolormesh(z_yz_mesh, y_yz_mesh,
                      getattr(self.geom, fld)[:, :, self.xind])
            a5.set_title(lbl + text_x)
            a5.set_xlabel(r'$z/\pi$')
            a5.set_ylabel(r'$y/\rho_{ref}$')

            a6 = figure.add_subplot(2, 3, 6)
            a6.pcolormesh(y_xy_mesh, x_xy_mesh,
                      getattr(self.geom, fld)[self.zind, :, :])
            a6.set_title(lbl + text_z)
            a6.set_xlabel(r'$y/\rho_{ref}$')
            a6.set_ylabel(r'$x/a$')

            figure.savefig(ppd, format='pdf', bbox_inches='tight')

        ppd.close()
