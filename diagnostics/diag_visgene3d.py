# -*- coding: utf-8 -*-

import numpy as np
from scipy.interpolate import CubicSpline
from vtk import vtk
import os
import utils.averages as averages
from diagnostics.diagnostic import Diagnostic
import gvec_to_gene
from cffi import FFI
from pathlib import Path
import gvec_to_gene
import h5py
from scipy.interpolate import interp1d

class Diagvisgene3d(Diagnostic):
    def __init__(self, avail_vars=None, specnames=None):
        super().__init__()
        self.name = '3D Visualization'
        self.tabs = ['GENE3D']
        self.help_txt = """ Plots 3D data for paraview visualization
        \n
        \ntheta ind : number of theta values, default 512
        \nphi ind: number of phi values, default 256
        \nGVEC File : name of GVEC file (def. W7X_2000_045_State_0000_00005800.dat)
        \nrms : calculate rms in time for a given x,theta,phi (def. False)
        \nx avg : x average, only makes sense for radial_depedence = F (def. False)
        \nt avg : time averaged (def. True)
        \next : name extension for hdf5 (def. None)
        \nWrite coordinates to hdf5 file (def. True) """
        self.avail_vars = avail_vars
        self.specnames = specnames

        self.opts = {"quant": {'tag': "Quant", 'values': None, 'files': ['mom', 'field']},
                     "spec": {'tag': "Spec", 'values': self.list_specnames()},
                     "GVEC": {'tag': "GVEC", 'values': None},
                     "thetaind": {'tag': "theta ind", 'values': None},
                     "phiind": {'tag': "phi ind", 'values': None},
                     "rms": {'tag': "RMS", 'values': [False, True]},
                     "x_avg": {'tag': "x avg", 'values': [False, True]},
                     "t_avg": {'tag': "t avg", 'values': [True, False]},
                     'ext': {'tag': 'Ext', 'values': None},
                     "write_coords": {'tag': "Write Coords", 'values': [True, False]}}
        self.set_defaults()

        self.options_gui = Diagnostic.OptionsGUI()


    def set_options(self, run_data, species):

        if self.opts['quant']['value'] is None:
            raise RuntimeError("No quantities given for contours")

        self.rms = self.opts['rms']['value']

        # radial values
        self.nx0 = run_data.pnt.nx0

        # phi values (here ny0)
        self.ny0 = self.opts['phiind']['value']
        if not self.ny0:
            self.ny0 = 256
        else:
            self.ny0 = int(self.ny0)

        # theta values (here nz0)
        self.nz0 = self.opts['thetaind']['value']
        if not self.nz0:
            self.nz0 = 256
        else:
            self.nz0 = int(self.nz0)

        # def create_grids and quantities needed
        self.x_grid = run_data.spatialgrid.x_a
        # they have 1 more point for the boundaries
        self.phi_grid = np.zeros((self.ny0 + 1))
        self.theta_grid = np.zeros((self.nz0 + 1))

        # Calculating the cartesian coordinates
        self.coord_vis_in_c = None

        if self.opts['write_coords']['value']:
          print("Staring calculating cartesian coordinates, this might take some time, so be patient")
          ffi = FFI()
          if self.opts['GVEC']['value'] is None:
             self.gvec = 'W7X_2000_045_State_0000_00005800.dat'
          else:
             self.gvec = str(self.opts['GVEC']['value'])

          nameg = "../../gvec_State_files/" + self.gvec
          gfile = ffi.new("char[]", nameg.encode())
          gvec_to_gene.init(gfile)

          theta_star  = np.zeros((self.ny0+1,self.nz0+1),dtype=np.double,order='F')
          phi         = np.zeros((self.ny0+1,self.nz0+1),dtype=np.double,order='F')
          theta       = np.zeros((self.ny0+1,self.nz0+1),dtype=np.double,order='F')
          cart        = np.zeros((3,self.ny0+1,self.nz0+1), dtype=np.double,order='F')
          cart_in_c   = ffi.cast("double(*)[]", cart.ctypes.data)
          self.coord_vis  = np.zeros((3,self.nx0,self.ny0+1,self.nz0+1),dtype=np.double,order='F')

          dalpha = 2*np.pi/run_data.pnt.n0_global/self.ny0
          dtheta = 2*np.pi/self.nz0

          for i in range(0,self.nx0):
            for j in range(0,self.ny0+1):
                alpha=j*dalpha
                for k in range(0,self.nz0+1):
                   th = -np.pi + k*dtheta
                   theta_star[j,k]    = th*run_data.pnt.sign_Bpol_CW
                   self.theta_grid[k] = th*run_data.pnt.sign_Bpol_CW
                   phi[j,k]           = alpha
                   self.phi_grid[j]   = alpha

            theta_star_in_c   = ffi.cast("double(*)[]", theta_star.ctypes.data)
            phi_in_c   = ffi.cast("double(*)[]", phi.ctypes.data)
            theta_in_c   = ffi.cast("double(*)[]", theta.ctypes.data)

            gvec_to_gene.coords(self.ny0+1,self.nz0+1,self.x_grid[i],theta_star_in_c,phi_in_c,theta_in_c,cart_in_c)

            for j in range(0,self.ny0+1):
              for k in range(0,self.nz0+1):
                 self.coord_vis[:,i,j,k] = cart[:,j,k]

          self.coord_vis_in_c = ffi.cast("double(*)[]", self.coord_vis.ctypes.data)
          gvec_to_gene.finalize()


        self.zint_field = np.zeros((run_data.pnt.nx0, run_data.pnt.ny0, self.nz0))
        self.pest_field = np.zeros((run_data.pnt.nx0, self.ny0, self.nz0))
        self.field = np.zeros((run_data.pnt.nx0, self.ny0+1, self.nz0+1))

        self.run_data = run_data

        self.get_spec_from_opts()

        self.geom = run_data.geometry

        self.get_needed_vars()

        self.vis = {}

        if self.opts['ext']['value'] is None:
            self.ext = ''
        else:
            self.ext = str(self.opts['ext']['value'])

        for file in self.needed_vars.keys():
            # loop over quaitites in that file
            for quant in self.needed_vars[file].keys():
                if self.needed_vars[file][quant]:
                    if file == 'field':
                        self.vis[quant] = []
                    else:
                        for spec in self.specnames:
                            self.vis[quant + '#' + spec] = []
        return self.needed_vars


    def execute(self, data, step, last_step):
        # loop over files
        for file in self.needed_vars.keys():
            # loop over quaitites in that file
            for quant in self.needed_vars[file].keys():
                # loop over quaitites in that file
                if self.needed_vars[file][quant]:
                    if file == 'field':
                        # no species dependency
                        data_in = getattr(getattr(data, file), quant)(step.time, getattr(step, file))
                        self.vis[quant].append(data_in)

                    else:
                        # spec dependent
                        for spec in self.specnames:
                            data_in = getattr(getattr(data, file + '_' + spec), quant)(step.time, getattr(step, file))
                            self.vis[quant + '#' + spec].append(data_in)


    def plot(self, time_requested, output=None, out_folder=None):

        output = str(out_folder)
        ffi = FFI()
        strl = 16
        arrc_type     = "S{}".format(strl)

        if self.opts['write_coords']['value']:
         file = h5py.File(output + '/coordinates.h5', 'w')
         file["/coords"] = self.coord_vis
         file.close()
         print("Done calculating cartesian coordinates")
        else:
         with h5py.File(output + '/coordinates.h5','r') as hf:
          self.coord_vis = np.array(hf.get('/coords'),dtype=np.double,order='F')
          self.coord_vis_in_c = ffi.cast("double(*)[]", self.coord_vis.ctypes.data)
          print("Done reading cartesian coordinates")


        x_grid = np.zeros((self.nx0, self.ny0 + 1, self.nz0 + 1))
        for i in range(0,self.run_data.pnt.nx0):
            x_grid[i, :, :] = self.x_grid[i]

        phi_grid = np.zeros((self.nx0, self.ny0 + 1, self.nz0 + 1))
        for j in range(0,self.ny0 + 1):
            phi_grid[:, j, :] = self.phi_grid[j]

        theta_grid = np.zeros((self.nx0, self.ny0 + 1, self.nz0 + 1))
        for k in range(0,self.nz0 + 1):
            theta_grid[:, :, k] = self.theta_grid[k]

        values         = np.array((self.nx0-1,self.ny0,self.nz0),dtype=np.intc,order='F')
        values_in_c   = ffi.cast("int*", values.ctypes.data)

        def z_interpolation(var_in):

            var_out = np.zeros((self.run_data.pnt.nx0, self.run_data.pnt.ny0, self.nz0))
            for i in range(0,self.run_data.pnt.nx0):
                for j in range(0,self.run_data.pnt.ny0):
                    cs = interp1d(self.run_data.spatialgrid.z, var_in[i, j, :], bounds_error=False, fill_value="extrapolate")
                    var_out[i, j, :] = cs(self.theta_grid[0:self.nz0])
            return var_out

        def map_to_y_domain(pos):
            mapped_pos = pos + 100*self.run_data.pnt.ly
            mapped_pos = mapped_pos % self.run_data.pnt.ly
            return mapped_pos

        def y_interpolation(var_in):
            var_out = np.zeros((self.run_data.pnt.nx0, self.ny0, self.nz0))
            for i in range(0,self.run_data.pnt.nx0):
                print('Calculating point number ' + str(i) + ' out of ' + str(self.run_data.pnt.nx0-1) + ' points in x')
                for z in range(0,self.nz0):
                    z_value = self.theta_grid[z]
                    cs = interp1d(self.run_data.spatialgrid.y, var_in[i, :, z], bounds_error=False, fill_value="extrapolate")
                    for j in range(0,self.ny0):
                        phi_value = self.phi_grid[j]
                        y_value = map_to_y_domain(self.geom.Cy[0]/self.run_data.pnt.rhostar*(self.geom.q[i]*z_value-phi_value))
                        var_out[i, j, z] = cs(y_value)
            return var_out

        def z_interpolation_xavg(var_in):
            var_out = np.zeros((self.run_data.pnt.ny0, self.nz0))
            for j in range(0,self.run_data.pnt.ny0):
                    cs = interp1d(self.run_data.spatialgrid.z, var_in[j, :], bounds_error=False, fill_value="extrapolate")
                    var_out[j, :] = cs(self.theta_grid[0:self.nz0])
            return var_out

        def y_interpolation_xavg(var_in):
            var_out = np.zeros((self.ny0, self.nz0))
            for z in range(0,self.nz0):
                z_value = self.theta_grid[z]
                cs = interp1d(self.run_data.spatialgrid.y, var_in[:, z], bounds_error=False, fill_value="extrapolate")
                for j in range(0,self.ny0):
                    phi_value = self.phi_grid[j]
                    y_value = map_to_y_domain(self.geom.Cy[0]/self.run_data.pnt.rhostar*(self.geom.q[0]*z_value-phi_value))
                    var_out[j, z] = cs(y_value)
            return var_out

        if self.opts['t_avg']['value']:
            for quant in self.vis:

                ind_str = quant.find('#')
                if ind_str == -1:
                    ttl = quant
                else:
                    ttl = quant[0:ind_str] + "_" + quant[ind_str + 1:]

                GridNames      = np.array(['radial', 'phi', 'theta_star'],dtype=np.dtype(arrc_type))
                GridNames_in_c = ffi.cast("char(*)[]", GridNames.ctypes.data)

                VarNames      = np.array(['field'],dtype=np.dtype(arrc_type))
                VarNames_in_c = ffi.cast("char(*)[]", VarNames.ctypes.data)

                if self.opts['x_avg']['value']:
                    print("Doing RMS of XAVG and TIME")

                    vis_tavg = np.sqrt(averages.mytrapz(np.mean(np.array(self.vis[quant])**2, axis=1), time_requested))
                    self.zint_field = z_interpolation_xavg(vis_tavg)
                    self.pest_field_xavg = y_interpolation_xavg(self.zint_field)
                    self.pest_field = np.zeros((self.run_data.pnt.nx0, self.ny0, self.nz0))
                    for i in range(0,self.run_data.pnt.nx0):
                        self.pest_field[i,:,:] = self.pest_field_xavg
                    self.field[:, 0:self.ny0, 0:self.nz0] = self.pest_field
                    self.field[:, 0:self.ny0, self.nz0] = self.field[:, 0:self.ny0, 0]
                    self.field[:, self.ny0, :] = self.field[:, 0, :]

                    values_vis = np.array([self.field]).reshape((1, self.nx0*(self.ny0+1)*(self.nz0+1)), order='F')
                    values_vis_in_c   = ffi.cast("double(*)[]", values_vis.ctypes.data)
                    name_val = output + '/' + ttl + self.ext + '_xtavg.vtu'
                    if os.path.exists(name_val):
                      os.remove(name_val)
                      print("The file exist, replacing the vtu file")
                    else:
                      print("The file does not exist, creating the vtu file")
                    FileName = ffi.new("char[]", name_val.encode())
                    gvec_to_gene.write_data_to_vtk(3,3,1,values_in_c,1,strl,VarNames_in_c,self.coord_vis_in_c,values_vis_in_c,FileName)

                    #write the grid into a different file
                    grid = np.array([x_grid, phi_grid, theta_grid]).reshape((3, self.nx0*(self.ny0+1)*(self.nz0+1)), order='F')
                    grid_in_c   = ffi.cast("double(*)[]", grid.ctypes.data)
                    name_val = output + '/' + 'grid' + self.ext + '.vtu'
                    if os.path.exists(name_val):
                      os.remove(name_val)
                      print("The file exist, replacing the vtu file")
                    else:
                      print("The file does not exist, creating the vtu file")
                    FileName = ffi.new("char[]", name_val.encode())
                    gvec_to_gene.write_data_to_vtk(3,3,3,values_in_c,1,strl,GridNames_in_c,self.coord_vis_in_c,grid_in_c,FileName)

                else:
                    if self.opts['rms']['value']:
                         vis_tavg = np.sqrt(averages.mytrapz(np.array(self.vis[quant])**2, time_requested))
                         print("Doing RMS in TIME")
                    else: 
                         vis_tavg = averages.mytrapz(np.array(self.vis[quant]), time_requested)
                         print("Doing TIME AVG")

                    self.zint_field = z_interpolation(vis_tavg)
                    self.pest_field = y_interpolation(self.zint_field)
                    self.field[:, 0:self.ny0, 0:self.nz0] = self.pest_field

                    self.field[:, 0:self.ny0, self.nz0] = self.field[:, 0:self.ny0, 0]
                    self.field[:, self.ny0, :] = self.field[:, 0, :]

                    values_vis = np.array([self.field]).reshape((1, self.nx0*(self.ny0+1)*(self.nz0+1)), order='F')
                    values_vis_in_c   = ffi.cast("double(*)[]", values_vis.ctypes.data)
                    name_val = output + '/' + ttl + self.ext + '_tavg.vtu'
                    if os.path.exists(name_val):
                      print("The file exist, replacing the vtu file")
                      os.remove(name_val)
                    else:
                      print("The file does not exist, creating the vtu file")
                    FileName = ffi.new("char[]", name_val.encode())
                    gvec_to_gene.write_data_to_vtk(3,3,1,values_in_c,1,strl,VarNames_in_c,self.coord_vis_in_c,values_vis_in_c,FileName)

                    #write the grid into a different file
                    grid = np.array([x_grid, phi_grid, theta_grid]).reshape((3, self.nx0*(self.ny0+1)*(self.nz0+1)), order='F')
                    grid_in_c   = ffi.cast("double(*)[]", grid.ctypes.data)
                    name_val = output + '/' + 'grid' + self.ext + '.vtu'
                    if os.path.exists(name_val):
                      print("The file exist, replacing the vtu file")
                      os.remove(name_val)
                    else:
                      print("The file does not exist, creating the vtu file")
                    FileName = ffi.new("char[]", name_val.encode())
                    gvec_to_gene.write_data_to_vtk(3,3,3,values_in_c,1,strl,GridNames_in_c,self.coord_vis_in_c,grid_in_c,FileName)
        else:
            print("Doing at a given X and T")
            zint_field_t = np.zeros((len(time_requested), self.run_data.pnt.nx0, self.run_data.pnt.ny0, self.nz0))
            pest_field_t = np.zeros((len(time_requested), self.run_data.pnt.nx0, self.ny0, self.nz0))

            for quant in self.vis:

                ind_str = quant.find('#')
                if ind_str == -1:
                    ttl = quant
                else:
                    ttl = quant[0:ind_str] + "_" + quant[ind_str + 1:]

                VarNames      = np.array(['field'],dtype=np.dtype(arrc_type))
                VarNames_in_c = ffi.cast("char(*)[]", VarNames.ctypes.data)

                GridNames      = np.array(['radial', 'phi', 'theta_star'],dtype=np.dtype(arrc_type))
                GridNames_in_c = ffi.cast("char(*)[]", GridNames.ctypes.data)

                for i_t, time in enumerate(time_requested):

                    zint_field_t[i_t, :, :, :] = z_interpolation(np.array(self.vis[quant])[i_t, :, :, :])
                    pest_field_t[i_t, :, :, :] = y_interpolation(zint_field_t[i_t, :, :, :])
                    self.field[:, 0:self.ny0, 0:self.nz0] = pest_field_t[i_t, :, :, :]

                    self.field[:, 0:self.ny0, self.nz0] = self.field[:, 0:self.ny0, 0]
                    self.field[:, self.ny0, :] = self.field[:, 0, :]

                    values_vis = np.array([self.field]).reshape((1, self.nx0*(self.ny0+1)*(self.nz0+1)), order='F')
                    values_vis_in_c   = ffi.cast("double(*)[]", values_vis.ctypes.data)
                    name_val = output + '/' + ttl + self.ext + '_' + str(i_t) + '.vtu'
                    if os.path.exists(name_val):
                      os.remove(name_val)
                      print("The file exist, replacing the vtu file")
                    else:
                      print("The file does not exist, creating the vtu file")
                    FileName = ffi.new("char[]", name_val.encode())
                    gvec_to_gene.write_data_to_vtk(3,3,1,values_in_c,1,strl,VarNames_in_c,self.coord_vis_in_c,values_vis_in_c,FileName)

                    #write the grid into a different file
                    grid = np.array([x_grid, phi_grid, theta_grid]).reshape((3, self.nx0*(self.ny0+1)*(self.nz0+1)), order='F')
                    grid_in_c   = ffi.cast("double(*)[]", grid.ctypes.data)
                    name_val = output + '/' + 'grid' + self.ext + '.vtu'
                    if os.path.exists(name_val):
                      os.remove(name_val)
                      print("The file exist, replacing the vtu file")
                    else:
                      print("The file does not exist, creating the vtu file")
                    FileName = ffi.new("char[]", name_val.encode())
                    gvec_to_gene.write_data_to_vtk(3,3,3,values_in_c,1,strl,GridNames_in_c,self.coord_vis_in_c,grid_in_c,FileName)
