# -*- coding: utf-8 -*-

from tkinter import END
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import h5py
import utils.averages as averages
import utils.aux_func as aux_func
from diagnostics.diagnostic import Diagnostic
from diagnostics.baseplot import Plotting


class DiagContoursgene3d(Diagnostic):
    def __init__(self, avail_vars=None, specnames=None):
        super().__init__()
        self.name = 'Contours'
        self.tabs = ['GENE3D']
        self.help_txt = """Contour plots in x and y
        \n
        \nz ind : comma separated z interval (e.g., -3,3), default all range
        \nFourier x : spectral in x direction (def. False)
        \nFourier y : spectral in y direction (def. False)
        \nSquare value : square quantity (def. False)
        \nAbs. value : absolute value (def. False)
        \nt avg : time averaged (def. False)
        \nSave h5 : save hdf5 file (def. True)
        \nSave pdf : save pdf file (def. False)"""

        self.avail_vars = avail_vars
        self.specnames = specnames

        self.opts = {"quant": {'tag': "Quant", 'values': None, 'files': ['mom', 'field']},
                     "spec": {'tag': "Spec", 'values': self.list_specnames()},
                     "zind": {'tag': "z ind", 'values': None},
                     "f_x": {'tag': "Fourier x", 'values': [False, True]},
                     "f_y": {'tag': "Fourier y", 'values': [False, True]},
                     "square": {'tag': "square value", 'values': [False, True]},
                     "abs": {'tag': "Abs. value", 'values': [False, True]},
                     't_avg': {'tag': "t avg", 'values': [False, True]},
                     'save_h5': {'tag': "Save h5", 'values': [True, False]},
                     "save_pdf": {'tag': 'Save pdf', 'values': [False, True]}}

        self.set_defaults()

        self.options_gui = Diagnostic.OptionsGUI()

    def set_options(self, run_data, specnames=None):

        if self.opts['quant']['value'] is None:
            raise RuntimeError("No quantities given for contours")

        # z position, unset or -1 means all
        zind = self.opts['zind']['value']
        if not zind or zind == -1:
            self.zind = -100, 100  # this will choose the minimum and maximum values
        else:
            self.zind = zind.split(',')
        self.zind_min, self.zind_max = aux_func.find_nearest_points(run_data.spatialgrid.z,
                                                                    float(self.zind[0]),
                                                                    float(self.zind[1]))
        # Fourier in x
        self.x_fourier = self.opts['f_x']['value']

        # Fourier in y
        self.y_fourier = self.opts['f_y']['value']

        self.run_data = run_data

        self.get_spec_from_opts()

        self.geom = run_data.geometry

        self.get_needed_vars()

        self.contour = {}

        for file in self.needed_vars:
            # loop over quaitites in that file
            for quant in self.needed_vars[file]:
                if self.needed_vars[file][quant]:
                    if file == 'field':
                        self.contour[quant] = []
                    else:
                        for spec in self.specnames:
                            self.contour[quant + '#' + spec] = []

        return self.needed_vars

    def execute(self, data, step, last_step):
        """ not much to do other than appending data """

        # loop over files
        for file in self.needed_vars:
            # loop over quaitites in that file
            for quant in self.needed_vars[file]:
                # loop over quaitites in that file
                if self.needed_vars[file][quant]:
                    if file == 'field':
                        # no species dependency
                        data_in = getattr(getattr(data, file), quant)(step.time, getattr(step, file))

                        if self.x_fourier:
                            if self.y_fourier:
                                data_xyz = np.fft.fftshift(np.absolute(np.fft.fft2(data_in, axes=(0, 1))), axes=(0, 1))
                            else:
                                data_xyz = np.fft.fftshift(np.absolute(np.fft.fft(data_in, axis=0)), axes=0)
                        else:
                            if self.y_fourier:
                                data_xyz = np.fft.fftshift(np.absolute(np.fft.fft(data_in, axis=1)), axes=1)
                            else:
                                data_xyz = data_in#*self.geom.jacobian.T/np.mean(self.geom.jacobian.T)

                        if self.opts['square']['value']: data_xyz = np.absolute(data_xyz)**2

                        data_xy = np.average(data_xyz[:, :, self.zind_min:self.zind_max + 1],axis=2)

                        self.contour[quant].append(data_xy)

                    else:
                        # spec dependent
                        for spec in self.specnames:

                            data_in = getattr(getattr(data, file + '_' + spec),
                                              quant)(step.time, getattr(step, file))

                            if self.x_fourier:
                                if self.y_fourier:
                                  data_xyz = np.fft.fftshift(np.absolute(np.fft.fft2(data_in, axes=(0, 1))), axes=(0, 1))
                                else:
                                  data_xyz = np.fft.fftshift(np.absolute(np.fft.fft(data_in, axis=0)), axes=0)
                            else:
                                if self.y_fourier:
                                  data_xyz = np.fft.fftshift(np.absolute(np.fft.fft(data_in, axis=1)), axes=1)
                                else:
                                  data_xyz = data_in#*self.geom.jacobian.T/np.mean(self.geom.jacobian.T)

                            if self.opts['square']['value']: data_xyz = np.absolute(data_xyz)**2

                            data_xy = np.average(data_xyz[:, :, self.zind_min:self.zind_max + 1],axis=2)

                            self.contour[quant + '#' + spec].append(data_xy)

    def plot(self, time_requested, output=None, out_folder=None):

        if output:
            output.info_txt.insert(END, "Contour\n")

        plotbase = Plotting()
        plotbase.titles.update(
                {"phi": r"$\phi$", "n": r"$n$", "u_par": r"u_{//}", "T_par": r"T_{//}",
                 "T_perp": r"T_{\perp}", "Q_es": r"$Q_{es}$", "Q_em": r"$Q_{em}$",
                 "Gamma_es": r"$\Gamma_{es}$", "Gamma_em": r"$\Gamma_{em}$"})

        if self.opts['f_x']['value']:
            x_lbl = r"$k_x\rho_{ref}$"
            x = np.fft.fftshift(self.run_data.spatialgrid.kx_fftorder)
        else:
            x_lbl = r"$x/a$"
            x = self.run_data.spatialgrid.x_a

        if self.opts['f_y']['value']:
            y_lbl = r"$k_y\rho_{ref}$"
            y = np.fft.fftshift(self.run_data.spatialgrid.ky)
        else:
            y_lbl = r"$y/\rho_{ref}$"
            y = self.run_data.spatialgrid.y

        is_f = self.opts['f_x']['value'] or self.opts['f_y']['value'] or self.opts['abs']['value']

        text = 'Avg. over z = [' + '{:.1f}'.format(
                self.run_data.spatialgrid.z[self.zind_min]) + ',' + '{:.1f}'.format(
                self.run_data.spatialgrid.z[self.zind_max - 1]) + ']'

        if self.opts['t_avg']['value']:
            # loop over file
            for quant in self.contour:

                if self.opts['save_pdf']['value']:
                    pp2d = PdfPages(str(out_folder) + '/contours_{}.pdf'.format(quant))
                    plt.ioff()

                ind_str = quant.find('#')
                if ind_str == -1:
                    ttl = plotbase.titles[quant]
                else:
                    ttl = plotbase.titles[quant[0:ind_str]] + " " + quant[ind_str + 1:]

                fig = plt.figure(figsize=(15, 10))
                ax_1 = fig.add_subplot(1, 1, 1)
                plotbase.plot_contour_quant(ax_1, fig, x, y,
                                            averages.mytrapz(self.contour[quant], time_requested),
                                            is_f, x_lbl, y_lbl, ttl,True)
                fig.suptitle(text)

                if self.opts['save_pdf']['value']:
                    fig.savefig(pp2d, format='pdf', bbox_inches='tight')
                    pp2d.close()
                else:
                    fig.show()

                if self.opts['save_h5']['value']:
                    file = h5py.File(out_folder/'contour_{}.h5'.format(quant), 'w')
                    file["/x"] = x
                    file["/y"] = y
                    file["/" + quant] = averages.mytrapz(self.contour[quant], time_requested)
                    file.close()
        else:

            for quant in self.contour:

                ind_str = quant.find('#')
                if ind_str == -1:
                    ttl = plotbase.titles[quant]
                else:
                    ttl = plotbase.titles[quant[0:ind_str]] + " " + quant[ind_str + 1:]

                if self.opts['save_pdf']['value']:
                    pp2d = PdfPages(str(out_folder) + '/contour_{}.pdf'.format(quant))
                    plt.ioff()
                    for i_t, time in enumerate(time_requested):

                        fig = plt.figure(figsize=(15, 10))
                        ax_1 = fig.add_subplot(1, 1, 1)
                        plotbase.plot_contour_quant(ax_1, fig, x, y, self.contour[quant][i_t], is_f,
                                                    x_lbl, y_lbl, ttl + ', t = {:.2f}'.format(time),True)
                        fig.suptitle(text)
                        fig.savefig(pp2d, format='pdf', bbox_inches='tight')
                    pp2d.close()
                else:
                    fig = plt.figure(figsize=(15, 10))
                    ax_1 = fig.add_subplot(1, 1, 1)
                    plotbase.plot_contour_quant_timeseries(ax_1, fig, x, y, self.contour[quant],
                                                           is_f, time_requested, x_lbl, y_lbl, ttl,True)
                    fig.suptitle(text)
                    fig.show()
