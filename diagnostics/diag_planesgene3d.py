# -*- coding: utf-8 -*-

from tkinter import END

import h5py
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import CubicSpline
from matplotlib.backends.backend_pdf import PdfPages
import utils.aux_func as aux_func
import utils.averages as averages
from diagnostics.baseplot import Plotting
from diagnostics.diagnostic import Diagnostic
from scipy.interpolate import interp1d


class DiagPlanes3d(Diagnostic):
    def __init__(self, avail_vars=None, specnames=None):
        super().__init__()
        self.name = 'Polidal Planes'
        self.tabs = ['GENE3D']
        self.help_txt = """ Plots vs theta,phi coordinates
        \n
        \nalpha ind : comma separated values to plot for alpha, default 0
        \ntheta ind : number of theta values, default 512
        \nphi ind: number of phi values, default 256
        \nx avg : x average, only makes sense for radial_depedence = F (def. False)
        \nfft : calculated n,m mode numbers (def. False)
        \nrms : calculate rms in time for a given x,theta,phi (def. False)
        \nSave h5 : save hdf5 file (def. True) """

        self.avail_vars = avail_vars
        self.specnames = specnames

        self.opts = {"quant": {'tag': "Quant", 'values': None, 'files': ['mom', 'field']},
                     "spec": {'tag': "Spec", 'values': self.list_specnames()},
                     "alphaind": {'tag': "alpha ind", 'values': None},
                     "thetaind": {'tag': "theta ind", 'values': None},
                     "phiind": {'tag': "phi ind", 'values': None},
                     "fft": {'tag': "FFT", 'values': [True, False]},
                     "x_avg": {'tag': "x avg", 'values': [False, True]},
                     "rms": {'tag': "RMS", 'values': [False, True]},
                     "save_h5": {'tag': "Save h5", 'values': [True, False]}}

        self.set_defaults()

        self.options_gui = Diagnostic.OptionsGUI()


    def set_options(self, run_data, species):

        if self.opts['quant']['value'] is None:
            raise RuntimeError("No quantities given for contours")

        # alpha values
        self.alphaind = self.opts['alphaind']['value']
        if not self.alphaind:
            self.alphaind = np.pi  # this will choose the minimum and maximum values
        self.alphaind = float(self.alphaind)

        # theta values
        self.thetaind = self.opts['thetaind']['value']
        if not self.thetaind:
            self.thetaind = 512  # this will choose the minimum and maximum values
        else:
            self.thetaind = int(self.thetaind)

        # phi values
        self.phiind = self.opts['phiind']['value']
        if not self.phiind:
            self.phiind = 256  # this will choose the minimum and maximum values
        else:
            self.phiind = int(self.phiind)

        self.rms = self.opts['rms']['value']

        #def create_grids and quantities needed
        self.phi_grid = np.linspace(0, 2*np.pi/run_data.pnt.n0_global, self.phiind, endpoint=False)
        self.theta_grid = np.linspace(-np.pi, np.pi, self.thetaind, endpoint=False)
        self.zint_field = np.zeros((run_data.pnt.nx0, run_data.pnt.ny0, self.thetaind))
        self.pest_field = np.zeros((run_data.pnt.nx0, self.phiind, self.thetaind))
        self.alpha = np.zeros((run_data.pnt.nx0, self.phiind, self.thetaind))

        self.run_data = run_data

        self.get_spec_from_opts()

        self.geom = run_data.geometry

        self.get_needed_vars()

        self.planes = {}

        for file in self.needed_vars:
            # loop over quaitites in that file
            for quant in self.needed_vars[file]:
                if self.needed_vars[file][quant]:
                    if file == 'field':
                        self.planes[quant] = []
                    else:
                        for spec in self.specnames:
                            self.planes[quant + '#' + spec] = []
        return self.needed_vars


    def execute(self, data, step, last_step):
        # loop over files
        for file in self.needed_vars:
            # loop over quaitites in that file
            for quant in self.needed_vars[file]:
                # loop over quaitites in that file
                if self.needed_vars[file][quant]:
                    if file == 'field':
                        # no species dependency
                        data_in = getattr(getattr(data, file), quant)(step.time, getattr(step, file))
                        self.planes[quant].append(data_in)

                    else:
                        # spec dependent
                        for spec in self.specnames:
                            data_in = getattr(getattr(data, file + '_' + spec), quant)(step.time, getattr(step, file))
                            self.planes[quant + '#' + spec].append(data_in)

    def plot(self, time_requested, output=None, out_folder=None):


        def z_interpolation(var_in):

            var_out = np.zeros((self.run_data.pnt.nx0, self.run_data.pnt.ny0, self.thetaind))
            for i in range(0, self.run_data.pnt.nx0):
                for j in range(self.run_data.pnt.ny0):
                    #cs = CubicSpline(self.run_data.spatialgrid.z, var_in[i, j, :])
                    cs = interp1d(self.run_data.spatialgrid.z, var_in[i, j, :], bounds_error=False, fill_value="extrapolate")
                    var_out[i, j, :] = cs(self.theta_grid)
            return var_out

        def map_to_y_domain(pos):
            mapped_pos = pos + self.run_data.pnt.ly
            mapped_pos = mapped_pos % self.run_data.pnt.ly
            return mapped_pos

        def y_interpolation(var_in):
            var_out = np.zeros((self.run_data.pnt.nx0, self.phiind, self.thetaind))
            alpha = np.zeros((self.run_data.pnt.nx0, self.phiind, self.thetaind))
            for i in range(0, self.run_data.pnt.nx0):
                print('Calculating point number ' + str(i+1) + ' out of ' + str(self.run_data.pnt.nx0) + ' points in x')
                for z in range(self.thetaind):
                    z_value = self.theta_grid[z]
                    cs = interp1d(self.run_data.spatialgrid.y, var_in[i, :, z], bounds_error=False, fill_value="extrapolate")
                    for j in range(self.phiind):
                        phi_value = self.phi_grid[j]
                        y_value = map_to_y_domain(self.geom.Cy[0]/self.run_data.pnt.rhostar*(self.geom.q[i]*z_value-phi_value))
                        var_out[i, j, z] = cs(y_value)
                        alpha[i, j, z] = self.geom.q[i]*z_value-phi_value
                        #if var_out[i,j,z] < 0:
                        #    print("y interplation negative", i,j,z)
                        #    stop
            return var_out, alpha


        def z_interpolation_xavg(var_in):

            var_out = np.zeros((self.run_data.pnt.ny0, self.thetaind))
            for j in range(self.run_data.pnt.ny0):
                    cs = interp1d(self.run_data.spatialgrid.z, var_in[j, :], bounds_error=False, fill_value="extrapolate")
                    var_out[j, :] = cs(self.theta_grid)
            return var_out

        def y_interpolation_xavg(var_in):
            var_out = np.zeros((self.phiind, self.thetaind))
            alpha = np.zeros((self.phiind, self.thetaind))
            for z in range(self.thetaind):
                z_value = self.theta_grid[z]
                cs = interp1d(self.run_data.spatialgrid.y, var_in[:, z], bounds_error=False, fill_value="extrapolate")
                for j in range(self.phiind):
                    phi_value = self.phi_grid[j]
                    y_value = map_to_y_domain(self.geom.Cy[0]/self.run_data.pnt.rhostar*(self.geom.q[0]*z_value-phi_value))
                    var_out[j, z] = cs(y_value)
                    alpha[j, z] = self.geom.q[0]*z_value-phi_value
            return var_out, alpha


        if output:
            output.info_txt.insert(END, "Poloidal Planes\n")

        plotbase = Plotting()
        plotbase.titles.update(
                {"phi": r"$\phi$", "n": r"$n$", "u_par": r"u_{//}", "T_par": r"T_{//}",
                 "T_perp": r"T_{\perp}", "Q_es": r"$Q_{es}$", "Q_em": r"$Q_{em}$",
                 "Gamma_es": r"$\Gamma_{es}$", "Gamma_em": r"$\Gamma_{em}$"})

        y = self.run_data.pnt.sign_Bpol_CW*self.theta_grid
        y_lbl = r"$\theta$"
        x = self.phi_grid
        x_lbl = r"$\phi$"
        is_f = False

        X_grid, Y_grid = np.meshgrid(y, x)

        if self.opts['fft']['value']:
            n = np.fft.fftfreq(self.phiind)*self.phiind*self.run_data.pnt.n0_global
            m = np.fft.fftfreq(self.thetaind)*self.thetaind
            N, M = np.meshgrid(n, m)
            print(self.phiind, self.run_data.pnt.n0_global, self.thetaind)
            print("n=", n)

            # loop over file
        for quant in self.planes:

                if self.opts['x_avg']['value']:
                    plane_tavg = np.sqrt(averages.mytrapz(np.mean(np.array(self.planes[quant])**2, axis=1), time_requested))
                    self.zint_field = z_interpolation_xavg(plane_tavg)
                    self.pest_field, self.alpha = y_interpolation_xavg(self.zint_field)
                    fig = plt.figure(figsize=(15, 10))
                    ax_1 = fig.add_subplot(1, 1, 1)
                    ind_str = quant.find('#')
                    if ind_str == -1:
                        ttl = 'RMS of ' + plotbase.titles[quant]
                    else:
                        ttl = 'RMS of ' + plotbase.titles[quant[0:ind_str]] + " " + quant[ind_str + 1:]
                    text = ' , ' + r'$\alpha = 0, $ ' + '{:.1f}'.format(self.alphaind)
                    plotbase.plot_contour_quant(ax_1, fig, x, y, self.pest_field, is_f, x_lbl, y_lbl, ttl + text)
                    ax_1.contour(Y_grid, X_grid, self.alpha, levels=[0, self.alphaind], colors='w')
                    fig.show()

                else:

                    plane_tavg = np.sqrt(averages.mytrapz(np.array(self.planes[quant])**2, time_requested)) if self.opts['rms']['value'] else averages.mytrapz(np.array(self.planes[quant]), time_requested)
                    self.zint_field = z_interpolation(plane_tavg)
                    self.pest_field, self.alpha = y_interpolation(self.zint_field)

                    ind_str = quant.find('#')
                    if ind_str == -1:
                        ttl = 'RMS of ' + plotbase.titles[quant] if self.opts['rms']['value'] else plotbase.titles[quant]
                    else:
                        ttl = 'RMS of ' + plotbase.titles[quant[0:ind_str]] + " " + quant[ind_str + 1:] if self.opts['rms']['value'] else plotbase.titles[quant[0:ind_str]] + " " + quant[ind_str + 1:]
                    fig = plt.figure()
                    ax = fig.add_subplot(111)
                    plotbase.plot_contour_quant_xseries(ax, fig, x, y, self.pest_field, False, self.run_data.spatialgrid.x_a, x_lbl, y_lbl, ttl)

                    if self.opts['save_h5']['value']:
                        file = h5py.File(out_folder / 'poloidal_planes_{}.h5'.format(quant), 'w')
                        file["/theta"] = y
                        file["/phi"] = x
                        file["/field"] = self.pest_field
                    file.close()

                    if self.opts['fft']['value']:

                        temp_nm = np.zeros((self.run_data.pnt.nx0, self.phiind, self.thetaind), dtype=complex)
                        for x in range(0, self.run_data.pnt.nx0):
                            temp_nm[x, :, :] = np.fft.fft2(self.pest_field[x, :, :])
                        fourier_nm = np.sum(np.absolute(temp_nm)**2, axis=0)
                        fourier_n = np.sum(fourier_nm, axis=1)
                        fourier_m = np.sum(fourier_nm, axis=0)

                        max_pos_n = np.argmax(fourier_n)
                        max_n = np.abs(n[max_pos_n])
                        max_positive_n_pos = np.argmax(n)
                        max_positive_n = n[max_positive_n_pos]

                        max_pos_m = np.argmax(fourier_m)
                        max_m = np.abs(m[max_pos_m])
                        max_positive_m_pos = np.argmax(m)
                        max_positive_m = m[max_positive_m_pos]

                        title = r"Mode analysis in n,m space"
                        v = np.linspace(0.0, 1.0, 11, endpoint=True)
                        fig = plt.figure()
                        plt.title(title)
                        plt.xlabel(r'$n$')
                        plt.ylabel(r'$m$')
                        plt.xlim(0,max_positive_n)
                        plt.ylim(0,max_positive_m) 
                        cax = plt.contourf(N, M, fourier_nm.T/np.max(fourier_nm), v)
                        fig.colorbar(cax, shrink=1, aspect=20, ticks=v)

                        title = rf"Mode analysis in n space, maximum n = {max_n}"
                        fig = plt.figure()
                        plt.title(title)
                        plt.xlabel(r'$n$')
                        plt.ylabel(r'$\Phi_n$')
                        plt.plot(np.fft.fftshift(n), np.fft.fftshift(fourier_n)/np.max(fourier_n), 'b--')
                        plt.axvline(x=max_n, color='k', linestyle=':')
                        plt.xlim(0,max_positive_n)

                        title = rf"Mode analysis in m space, maximum m = {max_m}"
                        fig = plt.figure()
                        plt.title(title)
                        plt.xlabel(r'$m$')
                        plt.ylabel(r'$\Phi_m$')
                        plt.plot(np.fft.fftshift(m), np.fft.fftshift(fourier_m)/np.max(fourier_m), 'b--')
                        plt.axvline(x=max_m, color='k', linestyle=':')
                        plt.xlim(0,max_positive_m) 

                    plt.show()
