from tkinter import END

import os
import h5py
import matplotlib.pyplot as plt
import numpy as np
import putils.averages as averages
import putils.fourier as fourier
from diagnostics.baseplot import Plotting
from diagnostics.diagnostic import Diagnostic

class DiagCorrelations(Diagnostic):
    # pylint: disable=invalid-name
    def __init__(self, avail_vars=None, specnames=None):
        super().__init__()
        self.name = 'Correlations'
        self.tabs = ['fluxtube']

        self.help_txt = """Plots correlation functions in x and y.\n
                        Saves z=0,+-pi and average over z.\n
                        \nSave pdf : save pdf file (def. False)
                        """

        self.avail_vars = avail_vars

        self.specnames = specnames

        self.opts = {"spec": {'tag': "Spec", 'values': self.list_specnames()}}

        self.set_defaults()
        self.save_meta_file = True
        self.save_meta_fname = 'correlations.h5'

        self.options_gui = Diagnostic.OptionsGUI()

    def set_options(self, run, specnames, out_folder):
        self.has_EM = any(x.pnt.electromagnetic for x in run.parameters)
        self.kx = run.spatialgrid[0].kx_pos
        self.ky = run.spatialgrid[0].ky

        self.flux = {}

        self.get_needed_vars(['field', 'mom'])

        self.get_spec_from_opts()
        
        if self.save_meta_file:
            self.setup_save_meta_h5(out_folder, run)
        
        return self.needed_vars

    def execute(self, data, parameters, geometry, spatialgrid,  step):

        xgrid,ygrid = fourier.xy_grids(data.field.phi(step.time,step.field,parameters),parameters)
        phi_xyz = fourier.fouriertransform_k2x_2D(data.field.phi(step.time,step.field,parameters),parameters)
        #plt.contourf(ygrid,xgrid,abs(phi_xyz[:,:,int(parameters.pardict['nz0']/2.0)])) 
        #plt.show()
        taux,tauy,cfunc2D = corr_func_2D(phi_xyz[:,:,int(parameters.pardict['nz0']/2.0)],phi_xyz[:,:,int(parameters.pardict['nz0']/2.0)],xgrid,ygrid)

        for spec in self.specnames:

            dens = getattr(getattr(data, 'mom_' + spec), "dens")(step.time, step.mom, parameters)
            T_par = getattr(getattr(data, 'mom_' + spec), "T_par")(step.time, step.mom, parameters)
            T_perp = getattr(getattr(data, 'mom_' + spec), "T_perp")(step.time, step.mom, parameters)
            u_par = getattr(getattr(data, 'mom_' + spec), "u_par")(step.time, step.mom, parameters)
           
        if self.save_meta_file:
            self.save_meta_h5(step.time)
                
    def setup_save_meta_h5(self, folder, run):
        if not os.path.isdir(folder):
            raise Exception(os.path.abspath(folder) + " does not exist or is undefined")

        self.h5_meta = os.path.join(folder, self.save_meta_fname)
        #self.nkx_2 = len(run.spatialgrid[0].kx_pos)
        #self.nky = len(run.spatialgrid[0].ky)
        #if os.path.isfile(self.h5_meta):
        #    os.remove(self.h5_meta)
        #if os.path.isfile(self.h5_meta):    
        #    # file exist, simply open it
        #    self.h5_meta = h5py.File(self.h5_meta, 'a')
        #else:
        #    # file does not exit 
        #    self.h5_meta = h5py.File(self.h5_meta, 'w')
        #    self.h5_meta.create_dataset('/time', (1,), maxshape=(None,), dtype='float64')
        #    self.h5_meta.create_dataset('/kx', (self.nkx_2,), maxshape=(self.nkx_2,))
        #    self.h5_meta.create_dataset('/ky', (self.nky,), maxshape=(self.nky,))
        #    
        #    for spec in self.specnames:
        #        self.h5_meta.create_dataset('/{}/Qes/ky'.format(spec), (1, self.nky), maxshape=(None, self.nky,))
        #        self.h5_meta.create_dataset('/{}/Qes/kx'.format(spec), (1, self.nkx_2), maxshape=(None, self.nkx_2,))
        #        self.h5_meta.create_dataset('/{}/Ges/ky'.format(spec), (1, self.nky), maxshape=(None, self.nky,))
        #        self.h5_meta.create_dataset('/{}/Ges/kx'.format(spec), (1, self.nkx_2), maxshape=(None, self.nkx_2,))
        #        self.h5_meta.create_dataset('/{}/Pes/ky'.format(spec), (1, self.nky), maxshape=(None, self.nky,))
        #        self.h5_meta.create_dataset('/{}/Pes/kx'.format(spec), (1, self.nkx_2), maxshape=(None, self.nkx_2,))
        #        
        #        if self.has_EM:
        #            self.h5_meta.create_dataset('/{}/Qem/ky'.format(spec), (1, self.nky), maxshape=(None, self.nky,))
        #            self.h5_meta.create_dataset('/{}/Qem/kx'.format(spec), (1, self.nkx_2), maxshape=(None, self.nkx_2,))
        #            self.h5_meta.create_dataset('/{}/Gem/ky'.format(spec), (1, self.nky), maxshape=(None, self.nky,))
        #            self.h5_meta.create_dataset('/{}/Gem/kx'.format(spec), (1, self.nkx_2), maxshape=(None, self.nkx_2,))
        #            self.h5_meta.create_dataset('/{}/Pem/ky'.format(spec), (1, self.nky), maxshape=(None, self.nky,))
        #            self.h5_meta.create_dataset('/{}/Pem/kx'.format(spec), (1, self.nkx_2), maxshape=(None, self.nkx_2,))
        #    self.meta_file_pos = 0
#
    def save_meta_h5(self, time):
        pass
        #self.meta_file_pos += 1
        #for spec in self.specnames:
        #    flux_step = self.flux[spec]
        #    self.h5_meta['/{}/Qes/ky'.format(spec)].resize((self.meta_file_pos), axis=0)
        #    self.h5_meta['/{}/Qes/ky'.format(spec)][-1,:] = flux_step['Qes']['ky']
        #    self.h5_meta['/{}/Ges/ky'.format(spec)].resize((self.meta_file_pos), axis=0)
        #    self.h5_meta['/{}/Ges/ky'.format(spec)][-1,:] = flux_step['Ges']['ky']
        #    self.h5_meta['/{}/Pes/ky'.format(spec)].resize((self.meta_file_pos), axis=0)
        #    self.h5_meta['/{}/Pes/ky'.format(spec)][-1,:] = flux_step['Pes']['ky']
        #    self.h5_meta['/{}/Qes/kx'.format(spec)].resize((self.meta_file_pos), axis=0)
        #    self.h5_meta['/{}/Qes/kx'.format(spec)][-1,:] = flux_step['Qes']['kx']
        #    self.h5_meta['/{}/Ges/kx'.format(spec)].resize((self.meta_file_pos), axis=0)
        #    self.h5_meta['/{}/Ges/kx'.format(spec)][-1,:] = flux_step['Ges']['kx']
        #    self.h5_meta['/{}/Pes/kx'.format(spec)].resize((self.meta_file_pos), axis=0)
        #    self.h5_meta['/{}/Pes/kx'.format(spec)][-1,:] = flux_step['Pes']['kx']
        #    if self.has_EM:        
        #        self.h5_meta['/{}/Qem/ky'.format(spec)].resize((self.meta_file_pos), axis=0)
        #        self.h5_meta['/{}/Qem/ky'.format(spec)][-1,:] = flux_step['Qem']['ky']
        #        self.h5_meta['/{}/Gem/ky'.format(spec)].resize((self.meta_file_pos), axis=0)
        #        self.h5_meta['/{}/Gem/ky'.format(spec)][-1,:] = flux_step['Gem']['ky']
        #        self.h5_meta['/{}/Pem/ky'.format(spec)].resize((self.meta_file_pos), axis=0)
        #        self.h5_meta['/{}/Pem/ky'.format(spec)][-1,:] = flux_step['Pem']['ky']
        #        self.h5_meta['/{}/Qem/kx'.format(spec)].resize((self.meta_file_pos), axis=0)
        #        self.h5_meta['/{}/Qem/kx'.format(spec)][-1,:] = flux_step['Qem']['kx']
        #        self.h5_meta['/{}/Gem/kx'.format(spec)].resize((self.meta_file_pos), axis=0)
        #        self.h5_meta['/{}/Gem/kx'.format(spec)][-1,:] = flux_step['Gem']['kx']
        #        self.h5_meta['/{}/Pem/kx'.format(spec)].resize((self.meta_file_pos), axis=0)
        #        self.h5_meta['/{}/Pem/kx'.format(spec)][-1,:] = flux_step['Pem']['kx']
        #self.h5_meta['/time'].resize((self.meta_file_pos), axis=0)
        #self.h5_meta['/time'][-1] = time
        #self.h5_meta.flush()
        
    def close_meta_h5(self):
        pass
        #self.h5_meta.close()

    def load_meta_h5(self, out_folder, time_requested):
        self.h5_meta = os.path.join(out_folder, self.save_meta_fname)
        self.h5_meta = h5py.File(self.h5_meta, 'a')
        
        tm = self.h5_meta['/time'][:]

        mask = np.in1d(tm, time_requested, assume_unique=True)
        ind_array= np.arange(0,len(tm))[mask]

        #for spec in self.specnames:
        #    self.flux[spec]['Ges']['kx']=self.h5_meta['/{}/Ges/kx'.format(spec)][ind_array,:]
        #    self.flux[spec]['Ges']['ky']=self.h5_meta['/{}/Ges/ky'.format(spec)][ind_array,:]
        #    self.flux[spec]['Qes']['kx']=self.h5_meta['/{}/Qes/kx'.format(spec)][ind_array,:]
        #    self.flux[spec]['Qes']['ky']=self.h5_meta['/{}/Qes/ky'.format(spec)][ind_array,:]
        #    self.flux[spec]['Pes']['kx']=self.h5_meta['/{}/Pes/kx'.format(spec)][ind_array,:]
        #    self.flux[spec]['Pes']['ky']=self.h5_meta['/{}/Pes/ky'.format(spec)][ind_array,:]
        #    if self.has_EM:
        #        self.flux[spec]['Gem']['kx']=self.h5_meta['/{}/Gem/kx'.format(spec)][ind_array,:]
        #        self.flux[spec]['Gem']['ky']=self.h5_meta['/{}/Gem/ky'.format(spec)][ind_array,:]
        #        self.flux[spec]['Qem']['kx']=self.h5_meta['/{}/Qem/kx'.format(spec)][ind_array,:]
        #        self.flux[spec]['Qem']['ky']=self.h5_meta['/{}/Qem/ky'.format(spec)][ind_array,:]
        #        self.flux[spec]['Pem']['kx']=self.h5_meta['/{}/Pem/kx'.format(spec)][ind_array,:]
        #        self.flux[spec]['Pem']['ky']=self.h5_meta['/{}/Pem/ky'.format(spec)][ind_array,:]
        #self.h5_meta.close()
        
    def plot(self, time_requested, output=None, out_folder=None, terminal=None):
        """ 
            Plot."""
        pass
#
#        if output:
#            output.info_txt.insert(END, "Flux spectra:\n")
#
#        self.plotbase = Plotting()
#        self.plotbase.titles={"Ges": r"$\Gamma_{es}$", "Qes": r"$Q_{es}$", "Pes": r"$\Pi_{es}$"}
#        if self.has_EM:
#                 self.plotbase.titles.update({"Gem": r"$\Gamma_{em}$", "Qem": r"$Q_{em}$", "Pem": r"$\Pi_{em}$"})
#
#        if self.save_meta_file:
#            self.load_meta_h5(out_folder, time_requested)
#
#        for spec in self.specnames:
#            fig = plt.figure(figsize=(6, 8))
#
#            ax_loglog_kx = fig.add_subplot(3, 2, 1)
#            ax_loglin_kx = fig.add_subplot(3, 2, 3)
#            ax_linlin_kx = fig.add_subplot(3, 2, 5)
#            ax_loglog_ky = fig.add_subplot(3, 2, 2)
#            ax_loglin_ky = fig.add_subplot(3, 2, 4)
#            ax_linlin_ky = fig.add_subplot(3, 2, 6)
#
#            spec_flux = self.flux[spec]
#            
#            for flux in self.plotbase.titles.keys():
#
#                flux_kx = averages.mytrapz(spec_flux[flux]['kx'], time_requested)
#                flux_ky = averages.mytrapz(spec_flux[flux]['ky'], time_requested)
#                # Mask negative flux values for solid lines
#
#                pos_flux_kx = np.ma.masked_where((flux_kx <= 0), flux_kx)
#                # Mask zero flux for dashed lines, this takes care of the Nyquist mode in kx
#                all_flux_kx = np.ma.masked_where((flux_kx == 0), flux_kx)
#
#                pos_flux_ky = np.ma.masked_where((flux_ky <= 0), flux_ky)
#                all_flux_ky = np.ma.masked_where((flux_ky == 0), flux_ky)
#
#                # log-log =plots, dashed lines for negative values
#                baselogkx, = ax_loglog_kx.plot(self.kx, pos_flux_kx, label=self.plotbase.titles[flux])
#                ax_loglog_kx.plot(self.kx, np.abs(all_flux_kx), ls="--", color=baselogkx.get_color())
#                baselogky, = ax_loglog_ky.plot(self.ky, pos_flux_ky, label=self.plotbase.titles[flux])
#                ax_loglog_ky.plot(self.ky, np.abs(all_flux_ky), ls="--", color=baselogky.get_color())
#
#                # lin-log plots, nothing fancy
#                baseloglinkx, = ax_loglin_kx.plot(self.kx, all_flux_kx, label=self.plotbase.titles[flux])
#                ax_loglin_kx.plot(self.kx, all_flux_kx*self.kx, ls="--", color=baseloglinkx.get_color())
#                baseloglinky, = ax_loglin_ky.plot(self.ky, all_flux_ky, label=self.plotbase.titles[flux])
#                ax_loglin_ky.plot(self.ky, all_flux_ky*self.ky, ls="--", color=baseloglinky.get_color())
#
#                # lin-lin plots, nothing fancy
#                ax_linlin_kx.plot(self.kx, all_flux_kx, label=self.plotbase.titles[flux])
#                ax_linlin_ky.plot(self.ky, all_flux_ky, label=self.plotbase.titles[flux])
#
#                str_out = "{} {} = {:.4f} (kx intg.) - {:.4f} (ky intg.)".format(spec, flux,
#                                                                                 np.sum(flux_kx),
#                                                                                 np.sum(flux_ky))
#                if output:
#                    output.info_txt.insert(END, str_out + "\n")
#                    output.info_txt.see(END)
#
#                if terminal:
#                    print(str_out)
#
#            # set things
#            ax_loglog_kx.loglog()
#            ax_loglog_kx.set_xlabel(r"$k_x \rho_{ref}$")
#
#            ax_loglog_ky.loglog()
#            ax_loglog_ky.set_xlabel(r"$k_y \rho_{ref}$")
#
#            ax_loglin_kx.set_xscale("log")
#            ax_loglin_ky.set_xscale("log")
#
#            # lin-lin plots, nothing fancy
#            ax_linlin_kx.set_xlim(left=0)
#            ax_linlin_kx.set_xlabel(r"$k_x \rho_{ref}$")
#
#            ax_linlin_ky.set_xlabel(r"$k_y \rho_{ref}$")
#
#            for ax in [ax_loglog_kx, ax_loglin_kx, ax_linlin_kx, ax_loglog_ky, ax_loglin_ky,
#                       ax_linlin_ky, ]:
#                # ax.set_ylabel(r"$<|A|^2>$")
#                ax.legend()
#            ax_loglog_ky.set_title("{}".format(spec))
#            ax_loglog_kx.set_title("{}".format(spec))
#            #            fig.tight_layout()
#            fig.show()

    def save(time_requested, output=None, out_folder=None):
        pass

#    def corr_func_2D(v1,v2,grid1,grid2,show_plot=False):
#        dt1=grid1[1]-grid1[0]
#        N=len(grid)
#        cfunc=np.zeros(N,dtype='complex')
#        for i in range(N):
#            i0=i+1
#            cfunc[-i0]=np.sum(np.conj(v1[-i0:])*v2[:i0])
#        tau=np.arange(N)
#        tau=tau*dt
#        max_corr=max(np.abs(cfunc))
#        corr_time=0.0
#        i=0
#        while corr_time==0.0:
#            if (abs(cfunc[i])-max_corr/np.e) > 0.0 and \
#               (abs(cfunc[i+1])-max_corr/np.e) <= 0.0:
#                slope=(cfunc[i+1]-cfunc[i])/(tau[i+1]-tau[i])
#                zero=cfunc[i]-slope*tau[i]
#                corr_time=(max_corr/np.e-zero)/slope
#            i+=1
#        if show_plot:
#            plt.plot(tau,cfunc,'x-')
#            ax=plt.axis()
#            plt.vlines(corr_time,ax[2],ax[3])
#            plt.show()
#        return cfunc,tau,corr_time

def corr_func_2D(v1,v2,xgrid,ygrid,show_plot=False):
        dx=xgrid[1]-xgrid[0]
        dy=ygrid[1]-ygrid[0]
        Nx=len(xgrid)
        Ny=len(ygrid)
        cfunc=np.zeros((Nx,Ny))
        for i in range(Nx):
            for j in range(Ny):
                i0=i+1
                j0=j+1
                cfunc[-i0,-j0]=np.sum( np.sum( np.conj(v1[-i0:,-j0:])*v2[:i0,:j0],axis = 0),axis=0)
        cfunc = cfunc / np.sum(np.sum(np.conj(v1[:,:])*v2[:,:],axis = 0),axis=0)
        cfuncx = np.sum(cfunc,axis=1)
        cfuncy = np.sum(cfunc,axis=0)
        taux=np.arange(Nx)
        taux=taux*dx
        tauy=np.arange(Ny)
        tauy=tauy*dy
        corr_time_x = 0.0
        max_corr_x=max(np.abs(cfuncx))
        max_corr_y=max(np.abs(cfuncy))
        corr_time_y = 0.0
        i=0
        while corr_time_x==0.0:
            if (abs(cfuncx[i])-max_corr_x/np.e) > 0.0 and \
               (abs(cfuncx[i+1])-max_corr_x/np.e) <= 0.0:
                slope=(cfuncx[i+1]-cfuncx[i])/(taux[i+1]-taux[i])
                zero=cfuncx[i]-slope*taux[i]
                corr_time_x=(max_corr_x/np.e-zero)/slope
            i+=1
        i=0
        while corr_time_y==0.0:
            if (abs(cfuncy[i])-max_corr_y/np.e) > 0.0 and \
               (abs(cfuncy[i+1])-max_corr_y/np.e) <= 0.0:
                slope=(cfuncy[i+1]-cfuncy[i])/(tauy[i+1]-tauy[i])
                zero=cfuncy[i]-slope*tauy[i]
                corr_time_y=(max_corr_y/np.e-zero)/slope
            i+=1
        if show_plot:
            plt.title('taux: '+str(corr_time_x)[:6]+' tauy: '+str(corr_time_y)[:6])
            plt.contourf(tauy,taux,cfunc)
            plt.colorbar()
            plt.show()
            plt.plot(taux,cfuncx,'x-')
            ax=plt.axis()
            plt.vlines(corr_time_x,ax[2],ax[3])
            plt.show()
            plt.plot(tauy,cfuncy,'x-')
            ax=plt.axis()
            plt.vlines(corr_time_y,ax[2],ax[3])
            plt.show()

        return taux,tauy,cfunc


def corr_func_1D(v1,v2,grid,show_plot=True):
        dt=grid[1]-grid[0]
        N=len(grid)
        cfunc=np.zeros(N,dtype='complex')
        for i in range(N):
            i0=i+1
            cfunc[-i0]=np.sum(np.conj(v1[-i0:])*v2[:i0])
        tau=np.arange(N)
        tau=tau*dt
        max_corr=max(np.abs(cfunc))
        corr_time=0.0
        i=0
        while corr_time==0.0:
            if (abs(cfunc[i])-max_corr/np.e) > 0.0 and \
               (abs(cfunc[i+1])-max_corr/np.e) <= 0.0:
                slope=(cfunc[i+1]-cfunc[i])/(tau[i+1]-tau[i])
                zero=cfunc[i]-slope*tau[i]
                corr_time=(max_corr/np.e-zero)/slope
            i+=1
        if show_plot:
            plt.plot(tau,cfunc,'x-')
            ax=plt.axis()
            plt.vlines(corr_time,ax[2],ax[3])
            plt.show()
        return cfunc,tau,corr_time

def corr_func_1D_full_domain(v1,v2,grid,show_plot=True):
        dt=grid[1]-grid[0]
        N=len(grid)
        print("N",N)
        cfunc=np.zeros(2*N-1)
        cfunc[N-1]=np.sum(np.conj(v1[:])*v2[:])
        for i in range(N-1):
            i0=i+1
            cfunc[-i0]=np.sum(np.conj(v1[-i0:])*v2[:i0])
            cfunc[N-1-i0]=np.sum(np.conj(v1[:-i0])*v2[i0:])
        tau=np.arange(2*N-1)
        tau=tau*dt
        tau = tau-tau[-1]/2.0
        max_corr=max(np.abs(cfunc))
        corr_time=0.0
        i=0
        while corr_time==0.0:
            if (abs(cfunc[i])-max_corr/np.e) > 0.0 and \
               (abs(cfunc[i+1])-max_corr/np.e) <= 0.0:
                slope=(cfunc[i+1]-cfunc[i])/(tau[i+1]-tau[i])
                zero=cfunc[i]-slope*tau[i]
                corr_time=(max_corr/np.e-zero)/slope
            i+=1
        if show_plot:
            plt.plot(tau,cfunc,'x-')
            ax=plt.axis()
            plt.vlines(corr_time,ax[2],ax[3])
            plt.show()
        print("corr_time",corr_time)
        return cfunc,tau,corr_time


