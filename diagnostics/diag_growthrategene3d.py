#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import h5py
from diagnostics.diagnostic import Diagnostic
from diagnostics.baseplot import Plotting
from scipy.signal import argrelextrema

class DiagGrowhrategene3d(Diagnostic):

    # pylint: disable=invalid-name
    def __init__(self, avail_vars=None, specnames=None):
        super().__init__()
        self.name = 'Growth rate'
        self.tabs = ['GENE3D']

        self.help_txt = """Growth rate diagnostic
                        \n
                        \nSave h5 : save hdf5 file (def. True)"""

        self.avail_vars = avail_vars
        self.specnames = specnames

        self.opts = {"save_h5": {'tag': 'Save h5', 'values': [True, False]}}

        self.set_defaults()

        self.options_gui = Diagnostic.OptionsGUI()

    def set_options(self, run_data, specnames=None):


        self.max_field = []
        self.position_of_max = []
        self.field_at_max = []
        self.geom = run_data.geometry
        self.run_data = run_data
        self.specnames = self.run_data.specnames
        self.get_needed_vars(['field'])

        return self.needed_vars

    def execute(self, data, step, last_step):

        if self.position_of_max == []:
            last_phi = getattr(getattr(data, 'field'), "phi")(last_step.time, last_step.field)
            self.position_of_max = np.unravel_index(np.argmax(last_phi, axis=None), last_phi.shape)

        phi = getattr(getattr(data, 'field'), "phi")(step.time, step.field)
        self.max_field.append(np.amax(np.abs(phi)))
       
        self.field_at_max.append(np.amax(np.abs(phi[self.position_of_max[0],self.position_of_max[1],self.position_of_max[2]])))

    def plot(self, time_requested, output=None, out_folder=None):

        self.plotbase = Plotting()

        log_max = np.log(self.max_field)

        after_rescaling = np.where(np.diff(self.max_field) < 0)[0] + 1
        after_rescaling = np.append(0, after_rescaling)
        before_rescaling = np.append(np.delete(after_rescaling - 1, 0), len(self.max_field)-1)

        mask = (after_rescaling != before_rescaling)
        after_rescaling = after_rescaling[mask]
        before_rescaling = before_rescaling[mask]

        gammas = np.zeros(len(after_rescaling))
        for i in np.arange(0, len(after_rescaling)):
            field_slice = log_max[after_rescaling[i]:before_rescaling[i]]
            time_slice = time_requested[after_rescaling[i]:before_rescaling[i]]
            A = np.vstack([time_slice, np.ones(len(time_slice))]).T
            gammas[i], _ = np.linalg.lstsq(A, field_slice, rcond=None)[0]

        avg_times = (time_requested[after_rescaling] + time_requested[before_rescaling])/2

        dt = np.diff(time_requested)
        log_diff = np.diff(log_max)
        gamma_positions = np.where(log_diff > 0)
        cleaned_log_diff = log_diff[gamma_positions]
        cleaned_dt = dt[gamma_positions]

        flying_gamma = cleaned_log_diff / cleaned_dt

        plt.figure(figsize=(10, 7.5), dpi=100)
        title = rf"Growth rate $\gamma$ over time"
        plt.title(title)
        plt.xlabel(r'time')
        plt.ylabel(rf'$\gamma$')
        plt.plot(time_requested[gamma_positions], flying_gamma, label=rf"Flying $\gamma$ = {flying_gamma[-1]:.03f}", linewidth=2, color='blue')
        try:
            plt.plot(avg_times, gammas, label=rf"Averaged over rescaling timerange $\gamma$ = {gammas[-2]:.03f}", linewidth=2, color='red')
        except:
            pass
        plt.legend(loc="lower right")

        #Omega diag:

        omegas = np.empty(len(after_rescaling))
        omegas[:] = np.nan
        flying_omegas = np.array([])
        glob_maxima_times = np.array([])
        fft_omega = np.array([])

        for i in np.arange(0, len(after_rescaling)):
            gamma = gammas[i]
            field_slice = self.field_at_max[after_rescaling[i]:before_rescaling[i]]
            time_slice  = time_requested[after_rescaling[i]:before_rescaling[i]]
            rel_time_slice = time_slice-time_slice[0]
            corrected_field = field_slice / np.exp(gamma*rel_time_slice)
    
            maxima  = argrelextrema(corrected_field, np.greater)
            maxima_times = time_slice[maxima]
            periods = np.diff(maxima_times)
            glob_maxima_times = np.append(glob_maxima_times, maxima_times[:-1] + periods/2)
            flying_omegas = np.append(flying_omegas, 1/periods)
            try:
                T = (maxima_times[-1] - maxima_times[0]) / (len(maxima_times) - 1)
                omegas[i] = 1/T
            except:
                pass

            #FFT part:
            npoints = len(field_slice)
            deletion_range = int(npoints/2)
            dt = time_slice[1]-time_slice[0]

            for j in np.arange(0, deletion_range):
                spectrum = np.fft.fft(field_slice[:npoints-j])
                freq = np.fft.fftfreq(len(spectrum), d=dt)
                max_loc = argrelextrema(abs(spectrum), np.greater)
                try:
                    fft_omega = np.append(fft_omega, freq[max_loc[0][0]])
                except:
                    pass

        average_fft_omega = np.average(fft_omega)

        start_point = 0.0
        first_element = int(len(flying_omegas) * start_point)
        average_omega = np.average(flying_omegas[first_element:])

        title = rf"Frequency $\omega$ over time"

        fig=plt.figure()
        plt.title(title)
        plt.xlabel(r'time')
        plt.ylabel('Frequency $\omega$')

        plt.plot(glob_maxima_times, flying_omegas, label= rf"Flying $\omega$", linewidth=2, color='blue')
        plt.plot([time_requested[0], time_requested[-1]], [average_omega, average_omega], ls="--", c=".2", label = rf"Averaged over last {(1-start_point)*100}% of time range $\omega$ = {average_omega:.04f}")
        plt.plot([time_requested[0], time_requested[-1]], [average_fft_omega, average_fft_omega], ls="--", color='red', label = rf"Averaged FFT $\omega$ = {average_fft_omega:.04f}")

        plt.legend(loc = "upper right")

        plt.show()

        if self.opts['save_h5']['value']:
            file = h5py.File(out_folder / 'growth_rate.h5', 'w')
            file["/time"] = time_requested[gamma_positions]
            file["/gamma"] = flying_gamma
            file.close()
