# -*- coding: utf-8 -*-
""" class containing the notebook with all tabs for the different diags

    Naming of the frames  inside the notebook MUST be consistent
"""
from tkinter import Frame
from tkinter.ttk import Notebook
from tkinter import Label, Button, Checkbutton, BooleanVar
import os
import inspect
import importlib
import diagnostics.diagnostic


class DiagFrame:
    def __init__(self, parent, grandparent):
        self.notebook = Notebook(parent)
        self.notebook.grid(row=0, column=0, rowspan=3, sticky="nsew", padx=10,
                           pady=10)

        self.flds = ["field", "field", "field", "nrg"]

        self.fluxtubeframe = Frame(self.notebook)
        self.notebook.add(self.fluxtubeframe, text="Flux-tube")
        self.fluxtube_diags = 0

        self.xglobalframe = Frame(self.notebook)
        self.notebook.add(self.xglobalframe, text="x-global")
        self.xglobal_diags = 0

        self.GENE3Dframe = Frame(self.notebook)
        self.notebook.add(self.GENE3Dframe, text="3-D")
        self.GENE3D_diags = 0

        self.nrgframe = Frame(self.notebook)
        self.notebook.add(self.nrgframe, text="nrg")
        self.nrg_diags = 0

        self.diag_list = []
        self.autoadd_diagnostic(grandparent)

    def autoadd_diagnostic(self, parent):
        """ Parses the diagnostic folder and adds files to the GUI"""

        for filename in os.listdir('diagnostics'):
            if filename[-3:] == '.py' and filename[0:-3] != 'diagnostic' and \
                    filename[0:-3] != 'baseplot' and filename[0:-3] != 'diagspace':
                # Go through all modules in the diagnostic package
                try:
                    mod = importlib.import_module("diagnostics.{}".format(filename[:-3]))
                except ImportError:
                    print("Error importing {}. Ignoring diagnostic".format(filename))
                    continue
                for name, obj in inspect.getmembers(mod, inspect.isclass):
                    # Find all classes that are inherited from Diagnostic base class
                    if issubclass(obj, diagnostics.diagnostic.Diagnostic) and name != "Diagnostic":
                        parent.diag_list.append(DiagFrameEntry(
                            obj(parent.sim.data.avail_vars, parent.sim.specnames), self, parent))


class DiagFrameEntry:
    def __init__(self, act_diag, diagframe, gui):
        self.diag = act_diag
        self.active = BooleanVar()
        self.active.initialize(False)
        for my_tab in self.diag.tabs:
            self.my_row = getattr(diagframe, my_tab+'_diags')
            self.my_frame = getattr(diagframe, my_tab+'frame')
            self.my_column = int(self.my_row/10)*3
            self.my_row = self.my_row - int(self.my_column/3*10)

            self.check = Checkbutton(self.my_frame, variable=self.active,
                                     command=lambda var_in=self.active, gui=gui:
                                     self.selector(var_in, gui))

            self.check.grid(row=self.my_row, column=self.my_column, sticky="nsew", padx=10, pady=10)

            self.label = Label(self.my_frame, text=self.diag.name)
            self.label.grid(row=self.my_row, column=self.my_column+1, sticky="nswe", padx=10, pady=10)

            self.button = Button(self.my_frame, text="Options",
                                 command=lambda diag=self.diag, gui=gui:
                                 self.diag.options_gui.startup(diag, gui) if self.active.get() else None)
            self.button.grid(row=self.my_row, column=self.my_column+2, sticky="nswe", padx=10, pady=10)

            try:
                setattr(diagframe, my_tab + '_diags', getattr(diagframe, my_tab + '_diags') + 1)
            except:  # TODO Is it a good idea to swallow all exceptions here?
                print("failure :(")

    def selector(self, switch, gui):
        """ this method activates or deactivates a diagnostic based on the
        value of the switch"""
        if not gui.sim.run:
            print("select runs")
            return
        if switch.get():
            # activate
            # we need to add the diagnostic to the list of active diags
            #    and set its default options
            self.diag.__reload__(gui.sim.data.avail_vars, gui.sim.run[0].specnames)
            gui.diag_active.append(self.diag)
        else:
            # deactivate
            gui.diag_active.remove(self.diag)
