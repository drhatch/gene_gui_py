#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# from data.base_file import GENEFile
from utils.loader import Loader
from diagnostics.diag_ballamp import DiagBallamp
from diagnostics.diag_contours import DiagContours
from diagnostics.diag_flux_spectra import DiagFluxSpectra
from diagnostics.diag_amplitude_spectra import DiagAmplitudeSpectra
from diagnostics.diag_shearing_rate import DiagShearingRate
from diagnostics.diag_srcmom import DiagSrcMom
from diagnostics.diag_fluxesgene3d import DiagFluxesgene3d
from diagnostics.diag_geometrygene3d import DiagGeometrygene3d
from utils.loader import Loader
from pathlib import Path
import utils.derivatives as der
from data.data import Data
from utils.run import Simulation
# from run import Run

import numpy as np
import matplotlib.pyplot as plt

# import h5py
# import numpy as np
# file='./tests/test_local_std/3spec/field_0001.h5'
# fid = h5py.File(file, 'r')
# data=fid.get('/field/phi/0000000000').value

# x=np.arange(0,6.28,0.1)
# y=np.sin(x)
# yp=der.compute_derivative(y,1)/0.1
# fig = plt.figure()
# ax = fig.add_subplot(111)
# ax.plot(x,y)
# ax.plot(x,yp)
# fig.show()

folder = './tests/test_local_std/3spec/'
folder = '/tmp/sshfs/bot/base_case_180/'
folder = Path('/home/merlo/work/GENE_gui_python/tests/test_gene3d/')
runextensions = ['_0001']
t_start = 0.0
t_end = 100.0

sim = Simulation(folder, folder, runextensions)
# act_run = Run(folder, runextensions[0])
# sim.run.append(act_run)
sim.prepare()

sim.data = Data()
sim.data.load_in(sim.run[0], sim.extensions)

selected_diags = []
# selected_diags.append(DiagBallamp(sim.data.avail_vars,sim.run[0].specnames))
# selected_diags.append(DiagFluxSpectra(sim.data.avail_vars, sim.run[0].specnames))
# selected_diags.append(DiagContours(sim.data.avail_vars,sim.run[0].specnames))
# selected_diags.append(DiagAmplitudeSpectra(sim.data.avail_vars,sim.run[0].specnames))
# selected_diags.append(DiagShearingRate(sim.data.avail_vars,sim.run[0].specnames))
selected_diags.append(DiagGeometrygene3d(sim.data.avail_vars, sim.run[0].specnames))
#
loader = Loader(selected_diags, sim.run[0], sim.data)
loader.set_interval(sim.data, t_start, t_end, 1)

for it, time in enumerate(loader.processable_times):
    print(" time {}".format(time))
    for i_d, diag in enumerate(selected_diags):
        diag.execute(sim.data, loader.processable[it])

for i_d, diag in enumerate(selected_diags):
    diag.plot(loader.processable_times)
